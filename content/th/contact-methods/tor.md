---
layout: page
title: Tor
author: mfc
language: th 
summary: วิธีการติดต่อ
date: 2021-09
permalink: /th/contact-methods/tor.md
parent: /th/
published: true
---
Tor เป็นเว็บบราวเซอร์ที่มุ่งเน้นความเป็นส่วนตัว ซึ่งช่วยให้คุณสามารถมีปฏิสัมพันธ์กับเว็บไซต์ต่างๆ อย่างเป็นนิรนาม โดยระบบจะไม่ให้ข้อมูลว่าคุณอยู่ที่ใด (ผ่านเลขที่อยู่ไอพีของคุณ) เวลาคุณเข้าเว็บไซต์เหล่านั้น 

แหล่งข้อมูลเพิ่มเติม: [ข้อมูลพื้นฐานเกี่ยวกับ Tor](https://www.torproject.org/about/overview.html.en)
