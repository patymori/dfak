---
name: SHARE CERT
website: https://www.sharecert.rs/
logo: SHARECERT_Logo.png
languages: Srpski, Македонски, English, Español
services: in_person_training, org_security, assessment, secure_comms, vulnerabilities_malware, browsing, account, harassment, forensic, legal, censorship
beneficiaries: journalists, hrds, activists, lgbti, women, youth, cso
hours: "Երկուշաբթիից ուրբաթ, 09:00 - 17:00, GMT+1/GMT+2"
response_time: "նույն օրվա ընթացքում"
contact_methods: web_form, email, pgp, mail, phone
web_form: https://www.sharecert.rs/prijava-incidenta/
email: info@sharecert.rs, emergency@sharecert.rs
pgp_key_fingerprint: info@sharecert.rs - 3B89 7A55 8C36 2337 CBC2 C6E9 A268 31E2 0441 0C10
mail: Kapetan Mišina 6A, Office 31, 11000 Belgrade, Serbia
phone: +381 64 089 70 67
initial_intake: yes
---

SHARE CERT-ի նպատակն իրենց համագործակիցներին օգնելն է՝ կիբեր հարձակումներին էֆեկտիվ պատասխան տալ, ինչպես նաև ապագա հարձակումների դեմ ավելի պաշտպանված լինել։
