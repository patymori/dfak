---
layout: page
title: Էլ.փոստի հասցե
author: mfc
language: hy
summary: Կապ հաստատելու միջոցներ
date: 2018-09
permalink: /hy/contact-methods/email.md
parent: /hy/
published: true
---

Ձեր նամակագրության բովանդակությունը, ինչպես նաև այն փաստը, որ դուք կապվել եք կազմակերպության հետ, կարող է տեսանելի լինել իշխանությունների կամ օրենսդիր մարմինների կողմից։
