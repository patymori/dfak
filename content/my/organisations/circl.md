---
name: Computer Incident Response Center Luxembourg
website: https://circl.lu
logo: circl.png
languages: English, Deutsch, Français, Luxembourgish
services: org_security, vulnerabilities_malware, forensic
beneficiaries: activists, lgbti, women, youth, cso
hours: "ရုံးအချိန်အတွင်း, UTC+2"
response_time: "၄ နာရီ"
contact_methods: web_form, email, pgp, mail, phone
web_form: https://www.circl.lu/contact/
email: info@circl.lu
pgp_key_fingerprint: CA57 2205 C002 4E06 BA70 BE89 EAAD CFFC 22BD 4CD5
phone: +352 247 88444
mail: 16, bd d'Avranches, L-1160 Luxembourg, Grand-Duchy of Luxembourg
initial_intake: yes
---

CIRCL သည် ပုဂ္ဂလိက၊ ကွန်မြူန်း၊ အစိုးအရမဟုတ်သော အဖွဲ့အစည်းများအတွက် CERT ဖြစ်ပါသည်။

CIRCL သည် Luxembourg ရှိ အသုံးပြုသူများ ၊ ကုမ္ပဏီများ ၊ အဖွဲ့အစည်းများအတွက် ဒစ်ဂျစ်တယ်တိုက်ခိုက်မှု ဖြစ်ပွားစဉ်များကို စိတ်ချယုံကြည်စွာ ကိုင်တွယ်ရှင်းလင်းပေးသော အဖွဲ့အစည်းဖြစ်ပါသည်။ ၎င်း၏ အဖွဲ့ဝင်ကျွမ်းကျင်သူများသည် မီးသတ်တပ်ဖွဲ့တစ်ခုလို အန္တရာယ်တစ်ခု သံသယဖြစ်သည်ဖြစ်စေ ၊ တွေ့ရှိသည်ဖြစ်စေ ၊ ဖြစ်ပေါ်သည်ဖြစ်စေ မြန်ဆန်ထိရောက်စွာ တုံ့ပြန်ပေးပါသည်။

CIRCL၏ ရည်ရွယ်ချက်မှာ ဆိုက်ဘာအန္တရာယ်များ ကို စည်းစနစ်ကျကျ စီစစ်၊ အစီရင်ခံ၊ တုံ့ပြန်ခြင်းတို့ပဲဖြစ်သည်။
