---
name: Digital Defenders Partnership
website: https://www.digitaldefenders.org
logo: DDP_logo_zwart_RGB.png
languages: Español, English, Français, Português, Deutsch, Nederlands, Русский
services: grants_funding, in_person_training, org_security, equipment_replacement, assessment, secure_comms, device_security, vulnerabilities_malware
beneficiaries: activists, journalists, hrds, lgbti, women, youth, cso
hours: "တနင်္လာမှကြာသာပတေး 9am-5pm CET"
response_time: "၄​ ရက်"
contact_methods: email, phone, mail
email: team@digitaldefenders.org
mail: Raamweg 16, 2596 HL Den Haag
phone: +31 070 376 5500
---

Digital Defenders Partnership (DDP) အဖွဲ့သည် ဒီဂျစ်တယ်ခြိမ်းခြောက်မှုများ ခံယူနေရသော လူ့အခွင့်အရေး ကာကွယ်သူများကို ထောက်ပံ့မှုပေးပြီး ဒေသတွင်း မြန်ဆန်သော တုံ့ပြန်မှုကွန်ယက်များကို ပိုမိုခိုင်မာစေရန် အားဖြည့်ပေးပါသည်။ DDP သည် လူ့အခွင့်အရေး ကာကွယ်သူများ၊​ သတင်းထောက်များ၊ အရပ်ဘက်လူ့အဖွဲ့အစည်းရှိ တက်ကြွလှုပ်ရှားသူများ နှင့် ဘလော့ဂ်ဂါများ စသည့် ပုဂ္ဂိုလ်များနှင့် အဖွဲ့အစည်းများအတွက် အရေးပေါ်ထောက်ပံ့မှုများကို စီစဥ်ဆောင်ရွက်ပေးပါသည်။

DDP တွင် အရေးပေါ်ကိစ္စများကို ဖြေရှင်းရန်အတွက် ရန်ပုံငွေအမျိုးအစား ၅ ခု နှင့် အဖွဲ့အစည်းတစ်ခုအတွင်း စွမ်းဆောင်ရည်မြှင့်တင်ရေးအတွက် ရေရှည်ထောက်ပံ့ကြေးများ ရှိပါသည်။ ထို့အပြင် ၎င်းတို့သည် အဖွဲ့အစည်းများအတွက်  ဒီဂျစ်တယ်လုံခြုံရေး နှင့် ပုဂ္ဂိုလ်လုံခြုံရေးဆိုင်ရာ သီးသန့်သင်တန်းများ နှင့် Rapid Response Network (မြန်ဆန်သော တုန့်ပြန်မှုကွန်ယက်) အစီအစဥ်များပါဝင်သည့်  Digital Integrity Fellowship ကို ဆောင်ရွက်ပေးပါသည်။
