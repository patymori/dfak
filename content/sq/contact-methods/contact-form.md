---
layout: page
title: Formulari i kontaktit
author: mfc
language: en
summary: Metodat e kontaktit
date: 2020-10
permalink: /en/contact-methods/contact-form.md
parent: /en/
published: true
---

Një formular kontakti ka të ngjarë të ruajë sekretin e mesazhit tuaj për organizatën marrëse, në mënyrë që vetëm ju dhe organizata marrëse të mund ta lexoni atë. Kjo do të ndodhë vetëm nëse faqja e internetit që hoston formularin e kontaktit zbaton masat e duhura të sigurisë, si p.sh. enkriptimin [TLS/SSL](https://ssd.eff.org/en/glossary/secure-sockets-layer-ssl) ndër të tjera , që është rasti me organizatat CiviCERT.

Megjithatë, fakti që ju keni vizituar faqen e internetit të organizatës ku gjendet formulari i kontaktit, ka të ngjarë të dihet nga qeveritë, agjencitë e zbatimit të ligjit ose palët e tjera me qasje në infrastrukturën lokale, rajonale ose globale të mbikëqyrjes. Nëse keni vizituar faqen e internetit të organizatës, kjo do të tregonte se mund të keni kontaktuar organizatën.

Nëse dëshironi të mbani privat faktin që keni vizituar faqen e internetit të organizatës (dhe potencialisht keni kontaktuar me to), atëherë preferohet të hyni në faqen e tyre përmes [Shfletuesit Tor](https://www.torproject.org/) ose një VPN ose proxy të besuar. Përpara se ta bëni këtë, merrni parasysh kontekstin ligjor në të cilin banoni dhe nëse keni nevojë të "mjegulloni" përdorimin tuaj të shfletuesit Tor duke [konfiguruar atë](https://tb-manual.torproject.org/running-tor-browser/) me një [transport anashkalues](https://tb-manual.torproject.org/circumvention/). Nëse po mendoni të përdorni një VPN ose proxy, [hulumtoni se ku është i vendosur ai server VPN ose proxy](https://protonvpn.com/blog/vpn-servers-high-risk-countries/) dhe [besimin tuaj në subjektin VPN](https://ssd.eff.org/en/module/choosing-vpn-thats-right-you).
