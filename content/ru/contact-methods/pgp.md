---
layout: page
title: PGP
author: mfc
language: ru
summary: Contact methods
date: 2020-11
permalink: /ru/contact-methods/pgp.md
parent: /ru/
published: true
---



Resources: [Access Now Helpline Community Documentation: Secure Email](https://communitydocs.accessnow.org/253-Secure_Email_Recommendations.html)

[Freedom of the Press Foundation: Encrypting Email with Mailvelope: A Beginner's Guide](https://freedom.press/training/encrypting-email-mailvelope-guide/)

[Privacy Tools: Private Email Providers](https://www.privacytools.io/providers/email/)
PGP (Pretty Good Privacy) и её аналог с открытым исходным кодом GPG (GNU Privacy Guard) — программы, позволяющие шифровать содержание электронных писем. Ни ваш почтовый провайдер, ни какое-либо другое лицо, получившее доступ к вашему сообщению, не смогут его прочесть. Однако сам факт того, что вы отправили письмо адресату, может стать известен государственным учреждениям и правоохранительным органам. Чтобы этого не допустить, лучше создать дополнительный адрес email, не связанный с вашей личностью.

Материалы [о безопасной электронной почте от службы цифровой поддержки Access Now](https://communitydocs.accessnow.org/253-Secure_Email_Recommendations.html).

- [Freedom of the Press Foundation, "Encrypting Email with Mailvelope: A Beginner's Guide"](https://freedom.press/training/encrypting-email-mailvelope-guide/)
- [Роскомсвобода, "Mailvelope: шифрование электронной почты"](https://safe.roskomsvoboda.org/mailvelope/)
- [Privacy Tools, "Private Email Providers"](https://www.privacytools.io/providers/email/)
- [Роскомсвобода, "ProtonMail — защищённая электронная почта"](https://safe.roskomsvoboda.org/protonmail/)
- [Роскомсвобода, "Tutanota — защищённая электронная почта"](https://safe.roskomsvoboda.org/tutanota/)
