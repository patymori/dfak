---
name: Qurium Media Foundation
website: https://www.qurium.org
logo: qm_logo.png
languages: English, Español, Français, Русский
services: web_hosting, web_protection, assessment, vulnerabilities_malware, ddos, triage, censorship, forensic
beneficiaries: journalists, media, hrds, activists, lgbti, cso
hours: с понедельника по воскресенье, 8:00 — 18:00 CET
response_time: 4 часа
contact_methods: email, pgp, web_form
web_form: https://www.qurium.org/contact/
email: info@virtualroad.org
pgp_key: https://www.virtualroad.org/keys/info.asc
pgp_key_fingerprint: 02BF 7460 09F9 40C5 D10E B471 ED14 B4D7 CBC3 9CF3
initial_intake: yes
---

Qurium Media Foundation — поставщик решений в области безопасности для независимых СМИ, правозащитных организаций, журналистов-расследователей и активистов. Qurium предлагает целый набор профессиональных, индивидуальных и безопасных решений с персональной поддержкой для организаций и лиц, подверженных риску. Этот набор включает:

- Безопасный хостинг с защитой от DDoS-атак для сайтов с повышенным риском
- Оперативная помощь организациям и частным лицам, которые находятся под непосредственной угрозой
- Аудит безопасности веб-сервисов и мобильных приложений
- Обход блокировок интернет-сайтов
- Глубокие расследования цифровых атак, мошеннических приложений, целевого вредоносного программного обеспечения, дезинформации