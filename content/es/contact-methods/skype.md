---
layout: page
title: Skype
author: mfc
language: es
summary: métodos de contacto
date: 2018-09
permalink: /es/contact-methods/skype.md
parent: /es/
published: true
---

Los gobiernos o los organismos de seguridad podrían acceder al contenido de tu mensaje, así como al hecho de que se contactó a la organización.
