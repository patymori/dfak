---
layout: page
title: Signal
author: mfc
language: es
summary: métodos de contacto
date: 2018-09
permalink: /es/contact-methods/signal.md
parent: /es/
published: true
---

El uso de Signal asegurará que el contenido de tu mensaje esté cifrado solo para la organización receptora, y solo tu y quien lo reciba sabréis que las comunicaciones tuvieron lugar. Ten en cuenta que Signal utiliza tanto tu número de teléfono como tu nombre de usuario, por lo que compartirás tu número de teléfono con la organización a la que te diriges.

Recursos: [Cómo utilizar Signal en Android](https://ssd.eff.org/es/module/c%C3%B3mo-utilizar-signal-en-android), [Cómo utilizar Signal en iOS](https://ssd.eff.org/es/module/c%C3%B3mo-utilizar-signal-en-ios), [Cómo usar la señal sin dar su número de teléfono (en inglés)](https://theintercept.com/2017/09/28/signal-tutorial-second-phone-number/)
